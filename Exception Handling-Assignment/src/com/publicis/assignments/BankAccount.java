package com.publicis.assignments;

import java.util.Scanner;

public class BankAccount {

	int accNo;
	String custName;
	String accType;
	float balance;

	public BankAccount(int accNo, String custName, String accType, float balance) {
		super();
		this.accNo = accNo;
		this.custName = custName;
		this.accType = accType;
		this.balance = balance;
	}
//deposit
	void deposit(float amt) throws NegativeAmount {

		if (amt < 0) {
			throw new NegativeAmount("Entered amount " + amt + " is not depositable");
		} else {
			balance = balance + amt;
			System.out.println("Your availabale balance is: " + balance);
		}

	}
	//with draw

	void withdraw(float amt) throws InsufficientFunds, NegativeAmount {

		if (amt > 0) {
			if (accType.equalsIgnoreCase("savings")) {
				if (balance >= 1000) {
					balance = balance - amt;
					System.out.println("Amount withdrawn success!!" + amt + "\nYour Remaining balance is: " + balance);
				}
			}

			if (accType.equalsIgnoreCase("current")) {
				if (balance >= 5000) {
					balance = balance - amt;
					System.out.println("Amount withdrawn success!!" + amt + "\nYour Remaining balance is: " + balance);
				}

				else {

					throw new InsufficientFunds("Insufficiant funds availabe!!!");
				}
			}
		} else {
			throw new NegativeAmount("You have entered negative amount.");
		}
	}
	//low balance

	float getBalance() throws LowBalanceException, InsufficientFunds {
		
		if (accType.equalsIgnoreCase("savings")) {
			if (balance >= 1000) {
				System.out.println("Your account balance is: " + balance);
				return balance;
			}
		}

		if (accType.equalsIgnoreCase("current")) {
			if (balance >= 5000) {
				System.out.println("Your account balance is: " + balance);
				return balance;
			}

			else {

				throw new LowBalanceException("Your balance is low!!!");
			}
		}
		return balance;
	} 
		
	public static void main(String[] args) throws NegativeAmount, InsufficientFunds, LowBalanceException {

		Scanner sc = new Scanner(System.in);
		System.out.println("Please enter account number");
		Integer accNo = sc.nextInt();

		Scanner sc1 = new Scanner(System.in);
		System.out.println("Please enter customer name");
		String custName = sc1.nextLine();

		Scanner sc2 = new Scanner(System.in);
		System.out.println("Please enter account type");
		String accType = sc2.nextLine();

		Scanner sc3 = new Scanner(System.in);
		System.out.println("Please enter amount");
		Integer amt = sc3.nextInt();

		BankAccount b = new BankAccount(accNo, custName, accType, amt);

		Scanner s = new Scanner(System.in);
		System.out.println("Please enter withdraw/deposit amount");
		Integer amount = s.nextInt();

		b.getBalance();
		b.deposit(amount);
		b.withdraw(amount);
		
	}
}
