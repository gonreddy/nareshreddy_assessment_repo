package com.publicis.assignments;

import java.util.Scanner;

public class CalcAverage {

	
	public double avgFirstN(int N) throws IllegalArgumentException{
				
			if(N<1) {
				
				throw new IllegalArgumentException("Entered number is not a natural number");
			}
			
			else {
				System.out.println(N+" It's as natural number!!"); 
				return N;
			}
	}
	
	public static void main(String[] args) {
		CalcAverage calAverageObj= new CalcAverage();

		Scanner sc=new Scanner(System.in);
		System.out.println("Please enter number");
		Integer num=sc.nextInt();

		calAverageObj.avgFirstN(num);
	}
		
}
