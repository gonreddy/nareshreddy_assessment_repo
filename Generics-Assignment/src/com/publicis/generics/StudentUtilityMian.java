package com.publicis.generics;


public class StudentUtilityMian {


	public static void main(String[] args) {
		
		StudentUtility<EnggStudent> engStudent1=new StudentUtility<EnggStudent>(new EnggStudent("Engineering Student1", 60, "2021"));
		StudentUtility<EnggStudent> engStudent2=new StudentUtility<EnggStudent>(new EnggStudent("Engineering Student2", 75, "2021"));
		
		StudentUtility<ScienceStudent> scienceStudent1=new StudentUtility<ScienceStudent>(new ScienceStudent("Science Student1", 70, "CSC"));
		StudentUtility<ScienceStudent> scienceStudent2=new StudentUtility<ScienceStudent>(new ScienceStudent("Science Student2", 80, "ECE"));
		StudentUtility<ScienceStudent> scienceStudent3=new StudentUtility<ScienceStudent>(new ScienceStudent("Science Student3", 80, "ECE"));
		
	System.out.println("Is student percentage equals: " +engStudent1.isStudentPercentageEqual(engStudent2));
	System.out.println("Is student percentage equals: " +scienceStudent2.isStudentPercentageEqual(scienceStudent3));			
	}
}
