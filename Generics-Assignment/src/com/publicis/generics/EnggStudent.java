package com.publicis.generics;

public class EnggStudent extends Student {

	private String passingYear;

	public EnggStudent(String name, double percent, String passingYear) {
		super(name, percent);
		this.passingYear = passingYear;
	}

	public String getPassingYear() {
		return passingYear;
	}

	public void setPassingYear(String passingYear) {
		this.passingYear = passingYear;
	}
	
}
