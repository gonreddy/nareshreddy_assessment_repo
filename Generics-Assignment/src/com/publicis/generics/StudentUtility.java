package com.publicis.generics;

import java.util.Comparator;

public class StudentUtility<T extends Student> {
	
	private T student;
	
	public StudentUtility(T student) {
		super();
		this.student = student;
	}

	public double getStudentPercent() {
		return student.getPercent();
		
	}
	
	public boolean isStudentPercentageEqual(StudentUtility<?> otherStudent) {
		
		return (student.getPercent()==otherStudent.getStudentPercent() ? true:false);
	}
}
