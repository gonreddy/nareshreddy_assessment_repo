package com.publicis.generics;

public class ScienceStudent extends Student{

	private String stream;

	public ScienceStudent(String name, double percent, String stream) {
		super(name, percent);
		this.stream = stream;
	}

	public String getStream() {
		return stream;
	}

	public void setStream(String stream) {
		this.stream = stream;
	}
	
}
