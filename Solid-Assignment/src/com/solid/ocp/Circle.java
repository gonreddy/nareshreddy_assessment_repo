package com.solid.ocp;

public class Circle extends Shape {

	
	private int radius;

	public int getRadius() {
		return radius;
	}

	public Circle(int radius) {
		super();
		this.radius = radius;
	}
    public void drawShape() {
    	System.out.println("---------------Circle--------------------");
    	double pi=3.74;
    	System.out.println("The radius of the circle::"+radius);
    	int circlearea=(int) (pi*radius*radius);
    	System.out.println("Area of a circle:"+circlearea);
}}
