package com.solid.ocp;

public class Square extends Shape {

	private int side;

	public int getSide() {
		return side;
	}

	public Square(int side) {
		super();
		this.side =side;
	}

	@Override
	public void drawShape() {
		System.out.println("----------------Square-------------------");
		System.out.println("side of square:"+side);
		int areaofsquare=side*side;
		System.out.println("Area of Square:"+areaofsquare);
	}
	

}
