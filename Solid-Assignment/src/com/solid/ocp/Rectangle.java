package com.solid.ocp;

public class Rectangle extends Shape {
	private int width;
	private int height;

	public int getWidth() {
		return width;
	}

	public int getHeight() {
		return height;
	}

	

	public Rectangle(int width, int height) {
		super();
		this.width = width;
		this.height = height;
	}
	@Override
	void drawShape() {
		System.out.println("----------------Rectangle-------------------");
		System.out.println("Width of Rectangle:"+width+" Height of a Rectangle:"+height);
         int Rectanglearea=width*height;
         System.out.println("Area of rectangle:"+Rectanglearea);
	}

}
