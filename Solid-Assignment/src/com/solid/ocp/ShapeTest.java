package com.solid.ocp;

public class ShapeTest {

	public static void main(String[] args) {
		draw(new Square(4));
		draw(new Rectangle(2,4));
		draw(new Circle(3));

	}

	private static  void draw(Shape shape) {
		
		shape.drawShape();
	}

}
