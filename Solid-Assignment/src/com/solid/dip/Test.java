package com.solid.dip;

public class Test {

	public static void main(String[] args) {
		Developer backenddev=new BackEndDeveloper();
		Developer frontenddev=new FrontEndDeveloper();
		
		Project back=new Project(frontenddev);
		Project front=new Project(frontenddev);
		back.dvelopement();
		front.dvelopement();

	}

}
