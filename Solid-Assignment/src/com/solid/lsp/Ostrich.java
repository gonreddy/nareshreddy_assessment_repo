package com.solid.lsp;

public class Ostrich extends Bird {
	public void fly() throws CantFlyException{
		throw new CantFlyException("Ostrich cant fly");
	}
}
