package com.solid.lsp;

public class BirdTest {

	public static void main(String[] args) {
		Crow crow=new Crow();
		Duck duck=new Duck();
		Bird ostrich=new Ostrich();
crow.fly();
duck.fly();
ostrich.fly();
	}

}
