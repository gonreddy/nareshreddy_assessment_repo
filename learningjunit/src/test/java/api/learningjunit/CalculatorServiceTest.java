package api.learningjunit;

import static org.junit.Assert.assertEquals;
import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({CalculatorServiceTest.class})
public class CalculatorServiceTest {
	
	
	CalculatorService calculatorService;
	
	@Test
	public void addTest() {
		
		assertEquals(2, calculatorService.add(1, 1));
		assertEquals(2, calculatorService.add(1, 0));
	}
}
